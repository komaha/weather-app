require('./bootstrap');

window.Vue = require('vue');

Vue.component('app', require('./App.vue').default);

import store from './store/index';

const app = new Vue({
    el: '#app',
    store,
});
