import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        weatherData: [],
        currentCityInfo: {},
    },

    getters: {
        weatherData(state) {
            return state.weatherData;
        },

        currentCityInfo(state) {
            return state.currentCityInfo;
        }
    },

    mutations: {
        setWeatherData(state, payload) {
            let addedCities = state.weatherData.map(city => city.name);
            if (!addedCities.includes(payload.name)) {
                state.weatherData.push(payload);
            }
        },

        setCurrentCityInfo(state, payload) {
            state.currentCityInfo = payload;
        }
    },

    actions: {
        setWeatherData({commit}, payload) {
            Axios.get('/api/getWeatherData', {
                params: {
                    city: payload.city,
                    apiKey: payload.apiKey,
                }
            }).then(response => {
                commit('setWeatherData', response.data);
            })
        },

        setCurrentCityInfo({commit}, payload) {
            commit('setCurrentCityInfo', payload);
        }
    },
});
