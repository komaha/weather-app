<?php
// MVC model
// Model's sole purpose is to process data into its permanent storage
// or seek and prepare data to be passed along to the other parts.

namespace App\Http\Classes;
use GuzzleHttp\Client;

class WeatherInfo
{
    public $apiKey;
    public $city;

    public function __construct(string $apiKey, string $city)
    {
        $this->apiKey = $apiKey;
        $this->city = $city;
    }

    public function getData()
    {
        $client = new Client([
            'base_uri' => 'http://api.openweathermap.org',
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', '/data/2.5/weather', [
            'query' => [
                'q' => $this->city,
                'appId' => $this->apiKey
            ]
        ]);

        $body = $response->getBody();

        return $body;
    }
}
