<?php
// MVC model
// Controller is the only part of the pattern the user should be interacting with.

namespace App\Http\Controllers;

use App\Http\Classes\WeatherInfo;
use Illuminate\Http\Request;

class GetWeatherDataController extends Controller
{
    public function index(Request $request) {
        $apiKey = $request->input('apiKey');
        $city = $request->input('city');

        $weatherInfo = new WeatherInfo($apiKey, $city);
        $data = $weatherInfo->getData();

        echo $data;
    }
}
